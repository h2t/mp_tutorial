global testTraj trajData

figure
axis([-2 2 -2 2]);
hold on;
tr = plot(trajData(:,2), trajData(:,3), 'b-', 'LineWidth', 2);
te = plot(testTraj(:,2), testTraj(:,3), 'r-.', 'LineWidth', 2);
legend([tr, te], {'Training Trajectory', 'Test Trajectory'})

figure
axis([0 1 -2 2]);
hold on
xp = plot(testTraj(:,1), testTraj(:,2), 'g-', 'LineWidth', 2);
yp = plot(testTraj(:,1), testTraj(:,3), 'm-', 'LineWidth', 2);
legend([xp,yp], {'x-axis', 'y-axis'})
