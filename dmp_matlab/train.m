addpath('dcp/');
global trajectory paras trainTraj Ws starts goals tau trajData

% 1: general dmp formulation
% 2: new dmp formulation with goal change
% 3: add tracking system and phase stopping (simulated real robot
% application)

trajData = processTrajectory(trajectory);
trainTraj = trajData;

rbf_num = 500;
dc = 1 / (rbf_num -1 );
centers = 1:-dc:0;
paras.kernelfcn = createKernelFcn(centers, 1);
paras.D = 200;
paras.K = paras.D^2/4;
paras.tau = 1;
paras.ax = -3;
paras.original_scaling = trajData(end,2:end) - trajData(1,2:end) + 1e-5;
Ws = dmptrain(trajData,paras);

starts = [trainTraj(1,2), trainTraj(1,3)];
goals = [trainTraj(end,2), trainTraj(end,3)];
tau = 1;